module BinarySearch

use import int.Int
use import int.ComputerDivision
use import ref.Ref
use import array.Array

exception Return int

let binarysearch (t : array int) (v : int) : int =
  requires {length t > 0 && (forall i. forall j. 0 <= i < j < length t -> t[i] <= t[j])}
  ensures {(forall i. 0 <= i < length t -> not t[i] = v ) <-> result = - 1} 
  ensures {(exists j. 0 <= j < length t && t[j] = v) <-> (not result = -1) && t[result] = v} 
  let l = ref 0 in
  let u = ref (length t - 1) in
  try
    while (!l <= !u) do
    invariant {!l >= 0 && !u < length t}
    invariant {forall i. 0 <= i < length t -> t[i] = v -> !l <= i <= !u }
    variant {!u - !l}
      let m = !l + div (!u - !l) 2 in
      if (t[m] < v)
      then l := m + 1
      else if (t[m] > v) 
           then u := m - 1
           else raise (Return m)
    done;
    -1
  with
    Return m -> m
  end

end
