module SelectionSort

use import int.Int
use import ref.Ref
use import array.Array

val swap (t : array int) (i j : int): unit
  requires { 0 <= i < length t && 0 <= j < length t}
  ensures {t[i] = old(t[j]) && t[j] = old(t[i])}

let selectionsort (t : array int) =
  requires {true}
  ensures {forall i. forall j. 0 <= i <= j < length t -> t[i] <= t[j]}
  for i = 0 to length t - 1 do
    invariant {forall k. forall l. 0 <= k <= l < i -> t[k] <= t[l]}
    invariant {forall a. forall b. 0 <= a < i <= b < length t -> t[a] <= t[b]}
    invariant {0 <= i <= length t}
    let min = ref i in
    for j = i + 1 to length t - 1 do
      invariant {(forall m. 0 <= m < i ->t[!min] >= t[m]) && (forall n. i <= n < j -> t[n] >= t[!min])}
      invariant {i < j <= length t}
      invariant {i <= !min < j}
      if (t[j] < t[!min])
      then min := j
    done;
    swap t i !min
  done

end 
