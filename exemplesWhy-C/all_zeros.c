
/*@ requires true;
  @ ensures true;
  @*/

int allzeros(int t[], int n) {
  int k = 0;
  
  while(k < n) {
    if (t[k]) return 0;
    k = k+1;
  }

  return 1;
}
