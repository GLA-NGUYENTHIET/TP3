
/*@ requires true;
  @ ensures true;
  @*/

void swap(int t[], int i, int j) {
  int tmp = t[i];
  t[i] = t[j];
  t[j] = tmp;
}


/*@ requires true;
  @ ensures true;
  @*/

void tri_selection(int t[], int n) {
  int i = 0;
  int w;

  while(i < n) {
    int min = i;
    int j = i+1;
    
    while (j < n) {
      if (t[j] < t[min]) {
	min = j;
      }
      j = j+1;
    }
    swap(t,i,min); 
    i = i+1;
  }
}
