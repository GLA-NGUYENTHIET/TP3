
int mintab(int t[], int n) {
  int m = t[0];
  int i;

  for(i = 1; i < n; i++) {
    if(t[i] < m) {
      m = t[i];
    }
  }

  return m;
}
