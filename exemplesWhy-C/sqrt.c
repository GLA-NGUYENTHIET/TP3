
/*@ requires true;
  @ ensures true;
  @*/

int sqrt(int a) {
  int i = 0;
  int tm = 1;
  int sum = 1;
  
  while (sum <= a) {      
    i++;
    tm=tm+2;
    sum=sum+tm;
  }
  
  return i;
}
