
/*@ requires true;
  @ ensures true;
  @*/

int binary(int t[], int n, int v) {
  int res = -1;
  int l = 0;
  int u = n-1;
  
  while (res == -1 && l <= u) {
    int m = l + (u - l) / 2;
    if (t[m] < v) {
      l = m + 1;
    }
    else if (t[m] > v) {
      u = m - 1;
    }
    else res=m; 
  }

  return res;
}

